from django.shortcuts import render
from .models import GroceryItem
from django.core.exceptions import ObjectDoesNotExist

def index(request):
    groceryitem_list = GroceryItem.objects.all()
    context = {'groceryitem_list': groceryitem_list}
    return render(request, "main/index.html", context)


def groceryitem(request, id):
    try:
        item = GroceryItem.objects.get(id=id)
        context = {'groceryitem': item}
    except ObjectDoesNotExist:
        context = {'groceryitem': None}    

    return render(request, 'main/groceryitem.html', context)